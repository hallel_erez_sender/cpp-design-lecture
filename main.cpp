#include <cstddef>
#include <cstdint>
#include <string>
#include <iostream>
#include <functional>

// API Design

bool annoying_print_range(size_t start, size_t end)
{
    if (start >= end)
    {
        return false;
    }

    for (size_t element = start; element < end; element++)
    {
        std::cout << element << std::endl;
    }

    return true;
}

void nice_print_range(size_t start, size_t element_count)
{
    for (size_t element = start; element < start + element_count; element++)
    {
        std::cout << element << std::endl;
    }
}

// Hiding Templates

template<class Element, size_t Capacity>
class AnnoyingQueue
{
public:
    bool push(const Element &element)
    {
        // Some logic here...
        return true;
    }

    bool pop(Element &o_element)
    {
        // Some logic here...
        return true;
    }

private:
    Element _elements[Capacity];
};

template<class Element, size_t Capacity>
void does_something_with_queue(AnnoyingQueue<Element, Capacity> &queue)
{
    // Some logic here...
}

template<class Element>
class Queue
{
public:
    virtual bool push(const Element &element) = 0;

    virtual bool pop(Element &o_element) = 0;
};

template<class Element, size_t Capacity>
class NiceQueue : public Queue<Element>
{
public:
    bool push(const Element &element) override
    {
        // Some logic here...
        return true;
    }

    bool pop(Element &o_element) override
    {
        // Some logic here...
        return true;
    }
};

template<class Element>
void does_something_with_queue(Queue<Element> &queue)
{
    // Some logic here...
}

// NVI

class AnnoyingParser
{
public:
    /**
     * Parses a file for some reason.
     *
     * @warning Please make sure to lock the file, you might cry otherwise.
     *
     * @param filename The path of the target file.
     *
     * @return true on success, false on error.
     */
    virtual bool parse(const std::string &filename) = 0;
};

class NiceParser
{
public:
    bool parse(const std::string &filename)
    {
        // Some logic that locks the file.
        bool result = parse_impl();
        // Some logic that releases the file.
        return result;
    }

protected:
    virtual bool parse_impl() = 0;
};

// Dependency Injection & Strategy

class AnnoyingLogger
{
public:
    virtual bool log(const std::string &message) = 0;
};

class ScreenLogger : public AnnoyingLogger
{
public:
    bool log(const std::string &message) override
    {
        // Some printing related logic here...
        return true;
    }
};

class FileLogger : public AnnoyingLogger
{
public:
    bool log(const std::string &message) override
    {
        // Some file related logic here...
        return true;
    }
};

class Writer
{
public:
    virtual bool write(const std::string &message) = 0;
};

class NiceLogger
{
public:
    explicit NiceLogger(Writer &writer) : _writer(writer)
    {
    }

    bool log(const std::string &message)
    {
        return _writer.write(message);
    }

private:
    Writer &_writer;
};

class ScreenWriter : public Writer
{
public:
    bool write(const std::string &message) override
    {
        // Some printing related logic here...
        return true;
    }
};

class FileWriter : public Writer
{
public:
    bool write(const std::string &message) override
    {
        // Some file related logic here...
        return true;
    }
};

class EvenNicerLogger
{
public:
    explicit EvenNicerLogger(Writer &initial_writer) : _writer(initial_writer)
    {
    }

    void change_writer(Writer &new_writer)
    {
        _writer = new_writer;
    }

    bool log(const std::string &message)
    {
        return _writer.get().write(message);
    }

private:
    std::reference_wrapper<Writer> _writer;
};

// The Dreaded Diamond & Virtual Inheritance

namespace annoying_army
{
    class Soldier
    {
    public:
        void exist()
        {
            // No logic required...
        }
    };

    class Programmer : public Soldier
    {
    };

    class Officer : public Soldier
    {
    };

    class TeamLead : public Officer, public Programmer
    {
    };

    void life_of_a_team_lead()
    {
        TeamLead lead;
        // lead.exist(); Calling this function will raise a "request for member exist is ambiguous" compilation error.
    }
}

namespace virtual_army
{
    class Soldier
    {
    public:
       Soldier()
       {
           std::cout << "My name is: ???" << std::endl;
       }

        explicit Soldier(const std::string &name)
        {
            std::cout << "My name is: " << name << std::endl;
        }

        void exist()
        {
            // No logic required...
        }
    };

    class Programmer : public virtual Soldier
    {
    public:
        explicit Programmer(const std::string &name) : Soldier(name)
        {
        }
    };

    class Officer : public virtual Soldier
    {
    public:
        explicit Officer(const std::string &name) : Soldier(name)
        {
        }
    };

    class TeamLead : public Officer, public Programmer
    {
    public:
        explicit TeamLead(const std::string &name) : Officer(name), Programmer(name)
        {
        }
    };

    void life_of_a_team_lead()
    {
        TeamLead lead("Hallel");
        lead.exist();
    }
}

int main()
{
}
